//
//  ApiService.swift
//  Weather_Widget
//
//  Created by Maksym Balukhtin on 8/11/18.
//  Copyright © 2018 Maksym Balukhtin. All rights reserved.
//

import Foundation
import Alamofire

class APIService{
    func getWeather(fromLat latitude: Double, andLong longitude: Double, completion: @escaping (WeatherModel?) -> Void){
        Alamofire.request("https://api.darksky.net/forecast/e8fb568f11355e97d5eb22746e2c910c/\(String(latitude)),\(String(longitude))", method: .get, parameters: nil, encoding: URLEncoding(destination: .queryString), headers: nil).responseJSON { (response) in
            switch response.result{
            case .success:
                do{
                    let result = try JSONDecoder().decode(WeatherModel.self, from: response.data!)
                    completion(result)
                }catch let error{
                    print(error)
                    completion(nil)
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion(nil)
            }
        }
    }
}
