//
//  SearchModel.swift
//  Weather_Test
//
//  Created by Maksym Balukhtin on 8/12/18.
//  Copyright © 2018 Maksym Balukhtin. All rights reserved.
//


import Foundation

struct SearchModel: Codable {
    let predictions: [Prediction]
    let status: String
}

struct Prediction: Codable {
    let description, id: String
    let structured_formatting: StructFormatting
}

struct StructFormatting: Codable{
    let main_text: String
    let secondary_text: String
}
