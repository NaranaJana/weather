//
//  TrulyCelsius.swift
//  Weather_Test
//
//  Created by Maksym Balukhtin on 8/11/18.
//  Copyright © 2018 Maksym Balukhtin. All rights reserved.
//

import Foundation

struct TrulyCelsius: Codable{
    var value: Double!
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let double = try? container.decode(Double.self){
            self.value = self.convertKelvinToCelsius(double)
        }
    }
    
    func convertKelvinToCelsius(_ parameter: Double) -> Double{
        return (parameter - 273.15)
    }
}
