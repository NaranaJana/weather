//
//  LaunchScreen.swift
//  Weather_Test
//
//  Created by Maksym Balukhtin on 8/10/18.
//  Copyright © 2018 Maksym Balukhtin. All rights reserved.
//

import UIKit
import MapKit

class LaunchScreen: UIViewController{
    let locationManager = CLLocationManager()
    override func viewDidLoad() {
        locationManager.requestWhenInUseAuthorization()
    }
}
