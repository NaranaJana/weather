//
//  DailyCell.swift
//  Weather_Test
//
//  Created by Maksym Balukhtin on 8/9/18.
//  Copyright © 2018 Maksym Balukhtin. All rights reserved.
//

import UIKit

class DailyCell: UITableViewCell, NibCell, ConfigurableCell{
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    
    static var nib: UINib! {
        return UINib(nibName: reuseId, bundle: nil)
    }
    
    static var reuseId: String! {
        return String(describing: self)
    }
    
    func config(for weather: List) {
        self.selectionStyle = .none
        let date = weather.dt_txt
        dayLabel.text = DatesFormatter.format(from: date, toFormat: "E")
        tempLabel.text = "\(KelvinToCelsius.convert(weather.main.temp_max))º/\(KelvinToCelsius.convert(weather.main.temp_min))º"
        guard let firstWeather = weather.weather.first else { return }
        weatherImageView.image = UIImage(named: firstWeather.icon)
        weatherImageView.tintColor = .black
    }
}
