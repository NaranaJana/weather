//
//  HourView.swift
//  Weather_Test
//
//  Created by Maksym Balukhtin on 8/9/18.
//  Copyright © 2018 Maksym Balukhtin. All rights reserved.
//

import UIKit

class HourView: UIView{
    @IBOutlet weak var hourLabel: UILabel?
    @IBOutlet weak var weatherImageView: UIImageView?
    @IBOutlet weak var tempLabel: UILabel?
    
    func setup(withHour hour: String, image: String, temp: String){
        hourLabel?.text = hour
        weatherImageView?.image = UIImage(named: image)
        tempLabel?.text = temp
    }
    
    static func view() -> HourView{
        return Bundle.main.loadNibNamed("HourView", owner: self, options: nil)?.first as! HourView
    }
}
