//
//  HourlyCell.swift
//  Weather_Test
//
//  Created by Maksym Balukhtin on 8/9/18.
//  Copyright © 2018 Maksym Balukhtin. All rights reserved.
//

import UIKit

class HourlyCell: UITableViewCell, NibCell, ConfigurableCell{
//    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollviewContentView: UIView!
    
    @IBOutlet weak var scrollViewContentViewWidthConstraint: NSLayoutConstraint!
    
    static var nib: UINib! {
        return UINib(nibName: reuseId, bundle: nil)
    }
    
    static var reuseId: String! {
        return String(describing: self)
    }
    
    func config(for weathers: [List]) {
        self.selectionStyle = .none
        
        weathers.forEach { [unowned self] (weather) in
            let date = weather.dt_txt
            let dateStr = DatesFormatter.format(from: date, toFormat: "HH")
            let view = HourView.view()
            view.setup(withHour: dateStr, image: weather.weather.first!.icon, temp: "\(KelvinToCelsius.convert(weather.main.temp))º")
            self.scrollviewContentView.addSubview(view)
        }
        
        scrollViewContentViewWidthConstraint.constant = CGFloat(weathers.count * 50)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        var x = 0.0
        let height = Double(self.contentView.bounds.size.height)
        let width = 50.0
        for view in scrollviewContentView.subviews{
            view.frame = CGRect(x: x, y: 0.0, width: width, height: height)
            x += width
        }
        
        scrollView.contentSize = scrollviewContentView.frame.size
        scrollView.contentOffset = .zero
    }
}
