//
//  SearchCell.swift
//  Weather_Test
//
//  Created by Maksym Balukhtin on 8/12/18.
//  Copyright © 2018 Maksym Balukhtin. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell, NibCell, ConfigurableCell{
    @IBOutlet weak var cityLabel: UILabel!
    
    static var nib: UINib! {
        return UINib(nibName: reuseId, bundle: nil)
    }
    
    static var reuseId: String! {
        return String(describing: self)
    }
    
    func config(for weather: Prediction) {
        cityLabel.text = "\(weather.structured_formatting.main_text), \(weather.structured_formatting.secondary_text)" 
    }
}
