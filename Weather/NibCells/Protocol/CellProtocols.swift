//
//  CellProtocols.swift
//  Weather_Test
//
//  Created by Maksym Balukhtin on 8/9/18.
//  Copyright © 2018 Maksym Balukhtin. All rights reserved.
//

import UIKit

protocol ConfigurableCell {
    associatedtype CellType
    
    func config(for weather: CellType)
}

protocol NibCell  {
    static var nib: UINib! { get }
    static var reuseId : String! { get }
}
